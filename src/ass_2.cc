#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/ssid.h"
#include "ns3/nix-vector-routing-module.h"
#include "ns3/wifi-net-device.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/yans-wifi-phy.h"
#include "ns3/spectrum-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/spectrum-wifi-helper.h"
#include <fstream>

using namespace ns3;

int
main(int argc, char* argv[])
{
    bool verbose = true;
    bool spectrum = false;
    bool interference = false;
    bool no_multimodel = false;
    std::string datarate = "20Mb/s";
    uint32_t staWifiPerAp = 1;
    uint32_t apWifi = 2;

    CommandLine cmd(__FILE__);
    cmd.AddValue("verbose", "Tell echo applications to log if true", verbose);
    cmd.AddValue("spectrum", "select ns3::SpectrumWifiPhy or ns3::YansWifiPhy", spectrum);
    cmd.AddValue("interference", "ping two separated APs with same datarate", interference);
    cmd.AddValue("no_multimodel", "select MultiModelSpectrumChannel or SingleModelSpectrumChannel", no_multimodel);

    cmd.Parse(argc, argv);

    if (verbose)
    {
        LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
        LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
    }
    // The underlying restriction of 18 is due to the grid position
    // allocator's configuration; the grid layout will exceed the
    // bounding box if more than 18 nodes are provided.
    if (staWifiPerAp + apWifi > 18)
    {
        std::cout << "WifiNodes should be 18 or less; otherwise grid layout exceeds the bounding box"
                  << std::endl;
        return 1;
    }

    NodeContainer p2pApNodes;
    p2pApNodes.Create(apWifi);

    PointToPointHelper pointToPoint;
    pointToPoint.SetDeviceAttribute("DataRate", StringValue("100Mbps"));
    pointToPoint.SetChannelAttribute("Delay", StringValue("2ms"));

    NetDeviceContainer p2pApDevices;
    p2pApDevices = pointToPoint.Install(p2pApNodes);

    NodeContainer wifiStaNodes1;
    wifiStaNodes1.Create(staWifiPerAp);
    NodeContainer wifiApNode1 = p2pApNodes.Get(0);

    NodeContainer wifiStaNodes2;
    wifiStaNodes2.Create(staWifiPerAp);
    NodeContainer wifiApNode2 = p2pApNodes.Get(1);

    YansWifiPhyHelper phyYans;
    SpectrumWifiPhyHelper spectrumPhy;
    
    if(spectrum && !no_multimodel){
        Ptr<MultiModelSpectrumChannel> spectrumChannel = CreateObject<MultiModelSpectrumChannel>();
        Ptr<FixedRssLossModel> lossModel = CreateObject<FixedRssLossModel>();
        lossModel->SetRss(0);
        spectrumChannel->AddPropagationLossModel(lossModel);
        Ptr<ConstantSpeedPropagationDelayModel> delayModel = CreateObject<ConstantSpeedPropagationDelayModel>();
        spectrumPhy.SetChannel (spectrumChannel);
        std::string errorModelType = "ns3::NistErrorRateModel";
        spectrumPhy.SetErrorRateModel(errorModelType);
    }else if(spectrum && no_multimodel){
        Ptr<SingleModelSpectrumChannel> spectrumChannel = CreateObject<SingleModelSpectrumChannel>();
        Ptr<FixedRssLossModel> lossModel = CreateObject<FixedRssLossModel>();
        lossModel->SetRss(0);
        spectrumChannel->AddPropagationLossModel(lossModel);
        Ptr<ConstantSpeedPropagationDelayModel> delayModel = CreateObject<ConstantSpeedPropagationDelayModel>();
        spectrumPhy.SetChannel (spectrumChannel);
        std::string errorModelType = "ns3::NistErrorRateModel";
        spectrumPhy.SetErrorRateModel(errorModelType);
    }else{
        YansWifiChannelHelper channelYans = YansWifiChannelHelper::Default();
        channelYans.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
        channelYans.AddPropagationLoss("ns3::FixedRssLossModel","Rss",DoubleValue(0));
        phyYans.SetChannel(channelYans.Create());
    }

    Ptr<MultiModelSpectrumChannel> spectrumChannel = CreateObject<MultiModelSpectrumChannel>();
    Ptr<FixedRssLossModel> lossModel = CreateObject<FixedRssLossModel>();
    lossModel->SetRss(0);
    spectrumChannel->AddPropagationLossModel(lossModel);
    Ptr<ConstantSpeedPropagationDelayModel> delayModel = CreateObject<ConstantSpeedPropagationDelayModel>();
    spectrumPhy.SetChannel (spectrumChannel);
    std::string errorModelType = "ns3::NistErrorRateModel";
    spectrumPhy.SetErrorRateModel(errorModelType);

    WifiMacHelper mac;
    Ssid netssid = Ssid("Net");
    Ssid appssid = Ssid("App");

    WifiHelper wifi;
    wifi.SetStandard(WIFI_STANDARD_80211n);

    NetDeviceContainer staDevices1;
    NetDeviceContainer staDevices2;
    NetDeviceContainer apDevices1;
    NetDeviceContainer apDevices2;

    phyYans.Set("ChannelSettings",StringValue("{1, 20, BAND_2_4GHZ, 0}"));
    spectrumPhy.Set("ChannelSettings",StringValue("{1, 20, BAND_2_4GHZ, 0}"));
    mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(netssid),"BeaconInterval", TimeValue (MicroSeconds (102400)));
    if(spectrum){
        apDevices1.Add(wifi.Install(spectrumPhy, mac, wifiApNode1));
    }else{
        apDevices1.Add(wifi.Install(phyYans, mac, wifiApNode1));
    }

    mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(netssid), "ActiveProbing", BooleanValue(false));
    if(spectrum){
        staDevices1.Add(wifi.Install(spectrumPhy, mac, wifiStaNodes1));
    }else{
        staDevices1.Add(wifi.Install(phyYans, mac, wifiStaNodes1));
    }

    phyYans.Set("ChannelSettings",StringValue("{3, 20, BAND_2_4GHZ, 0}"));
    spectrumPhy.Set("ChannelSettings",StringValue("{5, 20, BAND_2_4GHZ, 0}"));
    mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(appssid),"BeaconInterval", TimeValue (MicroSeconds (102400)));
    if(spectrum){
        apDevices2.Add(wifi.Install(spectrumPhy, mac, wifiApNode2));
    }else{
        apDevices2.Add(wifi.Install(phyYans, mac, wifiApNode2));
    }

    mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(appssid), "ActiveProbing", BooleanValue(false));
    if(spectrum){
        staDevices2.Add(wifi.Install(spectrumPhy, mac, wifiStaNodes2));
    }else{
        staDevices2.Add(wifi.Install(phyYans, mac, wifiStaNodes2));
    }

    MobilityHelper mobility;
    mobility.SetPositionAllocator("ns3::GridPositionAllocator",
                                  "MinX",
                                  DoubleValue(0.0),
                                  "MinY",
                                  DoubleValue(0.0),
                                  "DeltaX",
                                  DoubleValue(5.0),
                                  "DeltaY",
                                  DoubleValue(5.0),
                                  "GridWidth",
                                  UintegerValue(2),
                                  "LayoutType",
                                  StringValue("RowFirst"));

    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(wifiStaNodes1);
    mobility.Install(wifiStaNodes2);
    mobility.Install(wifiApNode1);
    mobility.Install(wifiApNode2);

    InternetStackHelper stack;
    Ipv4NixVectorHelper nixRouting;
    stack.SetRoutingHelper(nixRouting);
    stack.Install(wifiApNode1);
    stack.Install(wifiApNode2);
    stack.Install(wifiStaNodes1);
    stack.Install(wifiStaNodes2);

    Ipv4AddressHelper address;
    address.SetBase("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer p2pApInterfaces;
    p2pApInterfaces = address.Assign(p2pApDevices);

    address.SetBase("192.168.100.0", "255.255.255.0");
    Ipv4InterfaceContainer wifiStaInterfaces1;
    Ipv4InterfaceContainer wifiApInterface1;
    wifiApInterface1 = address.Assign(apDevices1);
    wifiStaInterfaces1 = address.Assign(staDevices1);

    address.SetBase("192.168.200.0", "255.255.255.0");
    Ipv4InterfaceContainer wifiStaInterfaces2;
    Ipv4InterfaceContainer wifiApInterface2;
    wifiApInterface2 = address.Assign(apDevices2);
    wifiStaInterfaces2 = address.Assign(staDevices2);

    NodeContainer all = wifiApNode1;
    all.Add(wifiApNode2);
    all.Add(wifiStaNodes1);
    all.Add(wifiStaNodes2);

    Address dest1;
    std::string protocol;
    dest1 = InetSocketAddress(wifiApInterface1.GetAddress(0), 1025);
    protocol = "ns3::UdpSocketFactory";

    OnOffHelper onoff1 = OnOffHelper(protocol, dest1);
    onoff1.SetConstantRate(DataRate(datarate));
    ApplicationContainer apps1 = onoff1.Install(wifiStaNodes1.Get(0));
    apps1.Start(Seconds(1));
    apps1.Stop(Seconds(11.0));
        
    if(interference){
        Address dest2;
        dest2 = InetSocketAddress(wifiApInterface2.GetAddress(0), 1025);
        OnOffHelper onoff2 = OnOffHelper(protocol, dest2);
        onoff2.SetConstantRate(DataRate(datarate));
        ApplicationContainer apps2 = onoff2.Install(wifiStaNodes2.Get(0));
        apps2.Start(Seconds(1));
        apps2.Stop(Seconds(11.0));
    }

    Ptr<FlowMonitor> flowMonitor;
    FlowMonitorHelper flowHelper;
    flowMonitor = flowHelper.InstallAll();

    if(spectrum && no_multimodel)
        spectrumPhy.EnablePcapAll("ass_2_spectrum");
    else if (spectrum && !no_multimodel)
        spectrumPhy.EnablePcapAll("ass_2_spectrum_multi");
    else
        phyYans.EnablePcapAll("ass_2_yans");

    Simulator::Stop(Seconds(20.0));
    auto start = std::chrono::high_resolution_clock::now();
    Simulator::Run();
    auto end = std::chrono::high_resolution_clock::now();

    std::ofstream outFile;
    if(spectrum && no_multimodel)
        outFile.open ("ass_2_flow_results_spectrum.txt");
    else if (spectrum && !no_multimodel)
        outFile.open ("ass_2_flow_results_spectrum_multi.txt");
    else
        outFile.open ("ass_2_flow_results_yans.txt");

    flowMonitor->CheckForLostPackets();
    Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowHelper.GetClassifier ());
    std::map<FlowId, FlowMonitor::FlowStats> stats = flowMonitor->GetFlowStats ();

    for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin (); i != stats.end (); ++i)
    {
        Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
        outFile << "Flow ID: " << i->first << " Src Addr " << t.sourceAddress << " Dst Addr " << t.destinationAddress << std::endl;
        outFile << "Tx Packets = " << i->second.txPackets << std::endl;
        outFile << "Rx Packets = " << i->second.rxPackets << std::endl;
        outFile << "Lost Packets = " << i->second.lostPackets << std::endl;
        outFile << "Throughput = " << i->second.rxBytes * 8.0 / 1024 / 1024  << " Mbps" << std::endl;
        outFile << "Mean Delay = " << (double) i->second.delaySum.GetMilliSeconds () / (double) i->second.rxPackets << " ms" << std::endl;
        if (i->second.rxPackets > 0) {
            outFile << "Mean Jitter = " << (double) i->second.jitterSum.GetMilliSeconds () / (double) i->second.rxPackets << " ms" << std::endl;
        }
        outFile << "Packet Loss Ratio = " << (double)i->second.lostPackets / (double) (i->second.txPackets + i->second.lostPackets) << std::endl;
        outFile << std::endl;
    }

    std::chrono::duration<double> duration = end - start;
    outFile << "SimulationTime: " << duration.count() << " seconds" << std::endl;
    outFile.close();
    Simulator::Destroy();
    return 0;
}