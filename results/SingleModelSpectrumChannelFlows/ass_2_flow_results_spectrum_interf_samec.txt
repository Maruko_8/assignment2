Flow ID: 1 Src Addr 192.168.100.2 Dst Addr 192.168.100.1
Tx Packets = 48828
Rx Packets = 47583
Lost Packets = 1245
Throughput = 196.036 Mbps
Mean Delay = 436.854 ms
Mean Jitter = 0.284639 ms
Packet Loss Ratio = 0.0248637

Flow ID: 2 Src Addr 192.168.200.2 Dst Addr 192.168.200.1
Tx Packets = 48828
Rx Packets = 48224
Lost Packets = 604
Throughput = 198.677 Mbps
Mean Delay = 171.834 ms
Mean Jitter = 0.275755 ms
Packet Loss Ratio = 0.0122188

SimulationTime: 33.6012 seconds
