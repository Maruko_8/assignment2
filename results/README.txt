Per i risultati di ogni modello basta andare nella cartella apposita. 
In ogni cartella ci sono almeno tre file. 

I nomi dei file sono auto-esplicativi:
-se contengono la parola "interf" significa che è una simulazione con interferenza
-se contengono la parola "samec" significa che il canale wifi utilizzato per tutti i dispositivi è uguale
-se contengono la parola "diffc" significa che il canale wifi utilizzato nella prima rete è il canale 1, mentre nella seconda è il canale 3
-se contengono la parola "diffc2" significa che il canale wifi utilizzato nella prima rete è il canale 1, mentre nella seconda è il canale 5
-se non contiene nessuna di queste parole non fa interferenza